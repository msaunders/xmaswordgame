import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';

/**
  A fun and festive word game. Sort of.
  The user is presented with a word. The user has to type the word into an input box before the timer runs out. The timer will be set per word.
*/

// Set up an array of words, plus some other states to kick things off
class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      words: 
      [ 
        'Presents',
        'Santa Claus',
        'Quality Street',
        'Family',
        'Turkey',
        'Merry Christmas',
        'Happy holidays!',
        'Pull a cracker',
        'Snow',
        'Christmas tree',
        'Jingle bells',
        'Pigs in blankets',
        'Happy New Year!',
        '25th December',
        'All I want for Christmas',
        'We wish you a merry Christmas',
        'Mistletoe and wine',
        'I wish it could be Christmas everyday',
        'Mulled wine',
        'Mince pies',
        'Sleigh bells'
      ],
      correct: 0,
      wrong: 0,
      unanswered: 0,
      timer: 5, // though not technically needed it ensures the visible counter starts at 5
    }
  }

  // When the component updates, check the state of the time
  // If it is zero this means the form has not been submitted
  // So, increment the unanswered counter by 1
  // Clear the countdown var
  // Call newWord and newTimer
  componentDidUpdate() {
    if(this.state.timer === 0){
      console.log("time's up!");
      this.setState({
        unanswered: this.state.unanswered+1
      })
      clearInterval(window.countdown);
      this.newWord();
      this.newTimer();
    }

    // Place focus on the input (HTML5 autofocus does not work with React)
    // We can't do this on mount because the input is initially not in the DOM
    // See: http://stackoverflow.com/a/28890330/874691
    ReactDOM.findDOMNode(this.refs.input).focus(); 
  }
  
  // When selecting a new word we first generate a number based on how many items are in our words array
  // Then we set a chosenWord state - this is the word the user must type correctly!
  newWord() {
    var randomWord = Math.floor(Math.random() * this.state.words.length);
    this.setState({
      chosenWord: this.state.words[randomWord]
    });

    // Set the state started to true, so we know the game has begun
    this.setState({
      started: true,
    });
  }
  
  // Ser the time to 5 (seconds)
  // Start the counter by placing a setInterval function on a window-scoped variable called "countdown". 
  // The counter will increment every 1 second, and reduce the time left accordingly.
  newTimer() {
    this.setState({
      timer: 5,
    })
    var self = this;
    var i = 5;

    window.countdown = setInterval(function () {
      i--;
      self.setState({
        timer: i
      })
    }, 1000);
  }
  
  // When the form is submitted we prevent it from actually submitting using preventDefault()
  // Next we get the word the user submitted from the input, using refs
  // We then just do some checking if it is correct, wrong or unanswered, and update the state accordingly.
  // Finally we clear the countdown, call newWord and newTimer and clear the input
  handleSubmit(e) {
    e.preventDefault();
    
    var submittedWord = this.refs.input.value;
    
    if(submittedWord === this.state.chosenWord){
      // Correct
      this.setState({
        correct: this.state.correct+1
      })
    } else if (submittedWord === ''){
      // Unanswered
      this.setState({
        unanswered: this.state.unanswered+1
      })
    } else {
      // Wrong
      this.setState({
        wrong: this.state.wrong+1
      })
    }
    
    clearInterval(window.countdown);
    this.newWord();
    this.newTimer();
    
    this.refs.input.value = '';
  }
  
  stopGame() {
    // Stop the countdown
    clearInterval(window.countdown);

    // Set the game as finished so we can display the results
    this.setState({
      finished: true
    })
  }
  
  render() {
    return(
      <ReactCSSTransitionGroup
        transitionName="container"
        transitionAppear={true}
        transitionEnter={false}
        transitionLeave={false}
        transitionAppearTimeout={1000}>

        <div className="container">
          <h1>A Fun and Festive Christmas Game!</h1>
          {
            ! this.state.started &&
            <div>
              <p>The game is simple - you will be shown a Christmassy word or phrase, and you have to type it out within the time given.</p>
              <p>All words and phrases are case-sensitive, and randomly selected, so you might get the same word twice in a row!</p>
            </div>
          }

          {
            this.state.started &&
            <div>
              <p className="word">{this.state.chosenWord}</p>
              <form onSubmit={this.handleSubmit.bind(this)}>
                <input ref="input" className="input" />
              </form>
              <p>{this.state.timer} seconds remaining</p>
            </div>
          }

          {
            ! this.state.started &&
            <button onClick={() => { this.newWord(), this.newTimer() }}>Start Game</button>
          }

          {
            this.state.started &&
            <button onClick={() => { this.stopGame() }}>Stop Game</button>
          }
          
          {
            this.state.finished &&
            <div className="results">
              <h2>Results</h2>
              <p>Awesome! Here is a breakdown of how you did:</p>
              <ul>
                <li>Correct: <b>{this.state.correct}</b></li>
                <li>Wrong: <b>{this.state.wrong}</b></li>
                <li>Skipped: <b>{this.state.unanswered}</b></li>
                <li>
                  <i className="fa fa-twitter-square"></i>
                  <a href={"http://twitter.com/share?text=I scored " + (this.state.correct) + " out of " + (this.state.correct + this.state.wrong + this.state.unanswered) + " on this really fun and festive Christmas word game. Play it here:&url=https://goo.gl/yLNjV4"}>Tweet my score</a>
                </li>
              </ul>
            </div>
          }
        </div>
      </ReactCSSTransitionGroup>
    )
  }
}

export default App;
